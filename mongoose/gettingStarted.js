// getting-started

// Now say we like fuzzy kittens and want to record every kitten we ever meet in MongoDB. The first thing we need to do is include mongoose in our project and open a connection to the 'mongoose_tut' database on our locally running instance of MongoDB.

var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/mongoose_tut');

// We have a pending connection to the test database running on localhost. We now need to get notified if we connect successfully or if a connection error occurs:

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function(){
    // We're connected!

    // With Mongoose, everything is derived from a Schema. Let's get a reference to it and define our kittens.
    // Each schema maps to a MongoDB collection and defines the shape of the documents within that collection.

    var kittySchema = new mongoose.Schema({name: String})

    // We've got a schema with one property, name, which will be a String. The next step is compiling our schema into a Model.

    /*
    var Kitten = mongoose.model('Kitten', kittySchema);
    */

    // A model is a class with which we construct documents. In this case, each document will be a kitten with properties and behaviors as declared in our schema. Let's create a kitten document representing the little guy we just met on the sidewalk outside:

    /*
    var silence = new Kitten({name: 'Silence'});

    console.log(silence.name);
    */

    // Kittens can meow, so let's take a look at how to add "speak" functionality to our documents:

    // NOTE: methods must be added to the schema before compiling it with mongoose.model()

    kittySchema.methods.speak = function(){
        var greeting = this.name
            ? 'Meow name is ' + this.name
            : 'I don\'t have a name';
        console.log(greeting);
    };

    var Kitten = mongoose.model('Kitten', kittySchema);

    // Functions added to the methods property of a schema get compiled into the Model prototype and exposed on each document instance:

    var fluffy = new Kitten({name: 'fluffy'});

    fluffy.speak(); // "Meow name is fluffy"

    // We have talking kittens! But we still haven't saved anything to MongoDB. Each document can be saved to the database by calling its save method. The first argument to the callback will be an error if any occured.

    // O metodo save é um método assincrono
    fluffy.save(function(err, fluffy){
        if(err) console.error(err);
        fluffy.speak();
    })

    // We can access all of the kitten documents through our Kitten model.

    Kitten.find(function(err, kittens){
        if(err) console.error(err);
        console.log(kittens);
    })

    // If we want to filter our kittens by name, Mongoose supports MongoDBs rich querying syntax.

    Kitten.find({name: /^fluff/}, function(err, array){
        if(err) console.error(err);
        console.log(array);
    })
})