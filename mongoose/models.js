var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/mongoose_tut');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function () {

    // Models are fancy constructors compiled from Schema definitions. An instance of a model is called a document. Models are responsible for creating and reading documents from the underlying MongoDB database.

    var tankSchema = new mongoose.Schema(
        { name: String, size: String },
        {
            timestamps: { createdAt: 'created_at' }
        });
    var Tank = mongoose.model('Tank', tankSchema);

    //  An instance of a model is called a document.

    /*
    var small = new Tank({ size: 'small' });
    small.save(function (err) {
        if (err) return handleError(err);
        // saved!
    });
    */

    // or

    Tank.create({ size: 'small' }, function (err, small) {
        if (err) return handleError(err);
        // saved!
    });

    // or, for inserting large batches of documents
    /*
    Tank.insertMany([{ size: 'small' }], function (err) {

    });
    */

    // Querying

    // Finding documents is easy with Mongoose, which supports the rich query syntax of MongoDB. Documents can be retreived using each models find, findById, findOne, or where static methods.


    setTimeout(function () { // meti dentro do setTimeout para deixar que o Tank.create acabasse a sua função, caso contrário não ia encontrar nada
        Tank.find({ size: 'small' })
            .where('created_at')
            .lt(Date.now())
            .exec(function (err, res) {
                if (err) console.error(err);
                console.log(res);
            });
    }, 500);


    // Deleting

    // Models have static deleteOne() and deleteMany() functions for removing all documents matching the given filter.

    /*
    Tank.deleteOne({ size: 'large' }, function (err) {
        if (err) return console.error(err);
        // deleted at most one tank document
    });
    */

   // Updating

   // Each model has its own update method for modifying documents in the database without returning them to your application. See the API docs for more detail.
   
   Tank.updateOne({ size: 'large' }, { name: 'T-90' }, function(err, res) {
     // Updated at most one doc, `res.modifiedCount` contains the number
     // of docs that MongoDB updated
   });
   
   // If you want to update a single document in the db and return it to your application, use findOneAndUpdate instead.

    // Change Streams
    //  https://mongoosejs.com/docs/models.html

})