var mongoose = require('mongoose');
var Schema = mongoose.Schema;

mongoose.connect('mongodb://localhost/mongoose_tut');

// Everything in Mongoose starts with a Schema. Each schema maps to a MongoDB collection and defines the shape of the documents within that collection.

var blogSchema = new Schema({
    title: String,
    author: String,
    body: String,
    comments: [{ body: String, date: Date }],
    date: { type: Date, default: Date.now },
    hidden: Boolean,
    meta: {
        votes: Number,
        favs: Number
    }
});

// If you want to add additional keys later, use the Schema#add method.

// Each key in our code blogSchema defines a property in our documents which will be cast to its associated SchemaType. For example, we've defined a property title which will be cast to the String SchemaType and property date which will be cast to a Date SchemaType. Keys may also be assigned nested objects containing further key/type definitions like the meta property above.

// The permitted SchemaTypes are:

// String
// Number
// Date
// Buffer
// Boolean
// Mixed
// ObjectId
// Array
// Decimal128
// Map

// Schemas not only define the structure of your document and casting of properties, they also define document instance methods

// Creating a model

var Blog = mongoose.model('Blog', blogSchema);

// Instance methods

// Instances of Models are documents. Documents have many of their own built-in instance methods. We may also define our own custom document instance methods too.

var animalSchema = new Schema({ name: String, type: String });

// assign a function to the "methods" object of our animalSchema


// this.model está na prototype chain e, penso eu, permite a qualquer documento invocar qualquer modelo que exista na base de dados
animalSchema.methods.findSimilarTypes = function (cb) {
    return this.model('Animal').find({ type: this.type }, cb)
}

// Now all of our animal instances have a findSimilarTypes method available to them.

var Animal = mongoose.model('Animal', animalSchema);

var dog = new Animal({ name: 'woof', type: dog });

dog.save(function (err, dog) {
    if (err) console.error(err);

    dog.findSimilarTypes(function (err, dogs) {
        console.log(dogs);
    })
})

// Do not declare methods using ES6 arrow functions (=>). Arrow functions explicitly prevent binding this, so your method will not have access to the document and the above examples will not work.

// Options

// Schemas have a few configurable options which can be passed to the constructor or set directly:

// new Schema({..}, options);

// // or

// var schema = new Schema({..});
// schema.set(option, value);

// Valid options:

//     autoIndex
//     bufferCommands
//     capped
//     collection
//     id
//     _id
//     minimize
//     read
//     writeConcern
//     safe
//     shardKey
//     strict
//     strictQuery
//     toJSON
//     toObject
//     typeKey
//     validateBeforeSave
//     versionKey
//     collation
//     skipVersioning
//     timestamps
//     selectPopulatedPaths
//     storeSubdocValidationError


// option: timestamps

// If set timestamps, mongoose assigns createdAt and updatedAt fields to your schema, the type assigned is Date.

// By default, the name of two fields are createdAt and updatedAt, customize the field name by setting timestamps.createdAt and timestamps.updatedAt.

const thingSchema = new Schema({ name: String }, { timestamps: { createdAt: 'created_at' } });
const Thing = mongoose.model('Thing', thingSchema);
const thing = new Thing({name: 'test'});
// !!! o código em baixo não está a funcionar como o esperado. Faz o save mas não faz os updates. Perceber o que se passa
thing.save(function (err, thing) {  // `created_at` & `updatedAt` will be included
    if (err) console.error(err);
    console.log(thing)
    // With updates, Mongoose will add `updatedAt` to `$set`
    Thing.updateOne({name: 'test'}, { $set: { name: 'Test1' } }), function (err, res) {
        console.log(res);
        // If you set upsert: true, Mongoose will add `created_at` to `$setOnInsert` as well
        Thing.findOneAndUpdate({name: 'Test1'}, { $set: { name: 'Test2' } }, function (err, res) {
            console.log(res);
        });
    };
}); 


