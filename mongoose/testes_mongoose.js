var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var animalSchema = new Schema({ name: String, type: String });

var testSchema = new Schema({name: String});

var Test = mongoose.model('Test', testSchema);

// assign a function to the "methods" object of our animalSchema
animalSchema.methods.findSimilarTypes = function (cb) {
    return this.model('Animal').find({ type: this.type }, cb);
};

var Animal = mongoose.model('Animal', animalSchema);
var dog = new Animal({ type: 'dog' });

console.dir(dog.model('gdgr'));